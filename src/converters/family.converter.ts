import { RowDataPacket } from 'mysql2';
import { IFamilyAccount } from '../models/account.model.js';
import { IAccountDB, IFamilyAccountDB } from '../models/account.db.model.js';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function rowDataPacketToModel(db_result: RowDataPacket[]) {
  throw new Error('not implemented ---------------------');
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export function familyModelToDb(family_account: IFamilyAccount): [IAccountDB, IFamilyAccountDB] {
  const { account_id = -1, currency, balance, status, context } = family_account;

  //parse account data
  const account_db: IAccountDB = {
    account_id: -1,
    agent_id: -1,
    currency,
    type: 'family',
    balance,
    status,
  };

  //parse family data
  const family_db: IFamilyAccountDB = {
    family_id: account_id,
    account_info_id: -1,
    context,
  };

  return [account_db, family_db];
}
