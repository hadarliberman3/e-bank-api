import { IAccount } from "../models/account.model";

// Individual
export const validateIndividualCreation = (account: IAccount): boolean => {
    return doesAccountExist(account);
}

// Transactions
// export const validateTransferB2B = (src: IAccount, dest: IAccount, amount: number): boolean => {
//     return doesAccountExist(src) && doesAccountExist(dest);
//     // Both Active
//     // Both Business
//     // Same currency
//     // Src has enough money
//     // Min src balance 10000 after transaction
//     // If both has same company ID amount is limited to 10000 else 1000
// }



// utils

export const doesAccountExist = (account: IAccount): boolean => {
    return !!account;
}

// export const isAccountActive = (account:IAccount) => {

// }


// Transaction


// export const transferB2BFX = (src: IAccount,dest: IAccount, amount: number):boolean => {
//     // Both Exist
//     // Both Active
//     // Both Business
//     // Src has enough money
//     // Min src balance 10000 after transaction
//     // If both has same company ID amount is limited to 10000 else 1000
// }

// export const transferB2I = (src: IAccount,dest: IAccount, amount: number):boolean => {
//     // Both Exist
//     // Both Active
//     // Src Business
//     // Dest Individual
//     // Same currency
//     // Src has enough money
//     // Min source balance 10000 after transaction
//     // Amount limited to up to 1000
// }

// export const transferF2B = (src: IAccount,dest: IAccount, amount: number):boolean => {
//     // Both Exist
//     // Both Active
//     // Family owners should be active
//     // Family owners are individual
//     // Src type family
//     // Dest Business
//     // Same currency to all owners src and dest
//     // Src has enough money
//     // Min source balance 5000 after transaction
//     // Amount limited to up to 5000
// }

// Family 

// export const createFamilyValidator = (family:IFamilyAccount,owner_tuples: [owner_id: number, amount: number][]):boolean => {
//     // Exist
//     // Active
//     // Type
//     // Currency
//     // Have enough money
// }

// export const addMembersToFamily = (family: IFamilyAccount, accounts_to_add: IAccount[], owner_tuples: any):boolean => {
//     // Exist family
//     // Exist accounts to add
//     // family active
//     // accounts active
//     // all account_to_add individual
//     // same currency family and accounts_to_add
//     // accounts to add Have balance minimum 1000 after transaction
// }

// export const deleteAccountsFromFamily = (family: IFamilyAccount, accounts_to_delete: IAccount[], owner_tuples: any):boolean => {
//     // Exist family
//     // Exist accounts
//     // Accounts belong to family
//     // Family account minimum balance after transfer to individual is 5000 unless we're removing
//     // everyone and then the minimum is 0
// }

// export const closeFamilyAccount = (family: IFamilyAccount):boolean => {
//     // Exist family
//     // Check active
//     // No family members
// }

// export const activeDeactiveAccount = (account: IAccount[], action):boolean => {
//     // Exists
//     // If action is active then check all inactive
//     // If action is deactive then check all active
//     // All accounts are not family accounts
// }
