/* eslint-disable @typescript-eslint/no-unsafe-argument */
import { RequestHandler } from 'express';
//import { IAccount } from '../models/account.model.js';
import account_service from '../services/account.service.js';

class AccountController {
  setStatus: RequestHandler = async (req, res) => {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    await account_service.setStatus(req.body.ids, req.body.status);

    res.status(200).json({ response: 'succsess' });
  };

  /*getAllBusinesses: RequestHandler = async (req, res) => {
    const stam = await 5;
   
    res.status(200).json({account:""});
  };*/
}

export default new AccountController();
