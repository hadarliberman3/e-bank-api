import { RequestHandler } from 'express';
import * as converter from '../converters/family.converter.js';
import { IFamilyAccount } from '../models/account.model.js';

class FamilyController {
  createFamily: RequestHandler = async (req, res) => {
    const stam = await 5;
    const family_db = converter.familyModelToDb(req.body as IFamilyAccount);
    console.log(stam);
    res.status(200).json(family_db);
  };
  getFamily: RequestHandler = async (req, res) => {
    const stam = await 5;
    const family_id = req.params.id;
    console.log(stam);
    console.log(`looking for family ${family_id}`);
    res.status(200).json({ status: 'final project' });
  };

  getAllFamillies: RequestHandler = async (req, res) => {
    const stam = await 5;
    console.log(stam);
    console.log(`get all famillies`);
    res.status(200).json({ status: 'final project' });
  };
}

export default new FamilyController();
