import mysql from 'mysql2/promise';

// Hadar and Or configuration
// export const connection = await mysql.createConnection({
//   host: 'localhost',
//   port: 3306,
//   user: 'root',
//   password: 'qwerty',
//   database: 'eBank',
// });

// Tal's configuration
export const connection = await mysql.createConnection({
  host: 'localhost',
  port: 3306,
  user: 'root',
  password: 'qwerty123',
  database: 'eBank',
});
