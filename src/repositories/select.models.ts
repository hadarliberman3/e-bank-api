export const INDIVIDUAL: string =
  'i.individual_id,' +
  'i.first_name,' +
  'i.last_name,' +
  'i.address_id,' +
  'i.account_info_id,' +
  'i.email,' +
  'i.person_id';

export const ACCOUNT: string =
  'a.account_id,' + 'a.currency,' + 'a.balance,' + 'a.status,' + 'a.agent_id ';

export const FAMILY_ACCOUNT: string =
  'f.context,f.account_info_id as `family_account_id`,' +
  'i.first_name,i.last_name,i.address_id,' +
  'i.account_info_id as `individual_account_id`,i.email,i.person_id,' +
  'ai.currency as `individual_currency`,' +
  'ai.balance as `individual_balance`,ai.status as `individual_status`,ai.agent_id as `individual_agent,' +
  'af.currency af `family_currency`,af.balance as `family_balance`,af.status as `family_status`,af.agent_id as `family_agent`';
