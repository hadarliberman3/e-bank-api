import { RowDataPacket } from 'mysql2';
import { connection } from '../db/sql.connection.js';
import { IFamilyAccountDB } from '../models/account.db.model.js';
import IndividualRepository from './individual.repository.js';
import { FAMILY_ACCOUNT } from './select.models.js';

class FamilyRepository {
  async createFamilyDB(family: IFamilyAccountDB): Promise<RowDataPacket[]> {
    const sql = 'INSERT INTO family_account SET ?';
    try {
      await connection.query(sql, family);
      return await this.getFamilyByIdDB(family.account_info_id as number);
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async addIndividualsToFamilyDB(
    individuals_acount_id: number[],
    family_acount_id: number,
  ): Promise<RowDataPacket[]> {
    try {
      let family_individual_array: number[] = [];
      for (let id of individuals_acount_id) {
        family_individual_array.push(family_acount_id, id);
      }
      const sql = `INSERT INTO family_individual (family_id,individual_id) ${
        'VALUES(?,?)' + ',(?,?)'.repeat(individuals_acount_id.length - 1)
      }`;
      await connection.query(sql, [
        family_individual_array.shift(),
        family_individual_array.shift(),
        ...family_individual_array,
      ]);
      const individuals_array: RowDataPacket[] = await IndividualRepository.getIndividualsByIdDB(
        individuals_acount_id,
      );
      return individuals_array;
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async getFamilyWithOwnersByIdDB(family_account_id: number): Promise<RowDataPacket[]> {
    console.log(family_account_id);
    const sql =
      `SELECT ${FAMILY_ACCOUNT} FROM family_account as f ` +
      'INNER JOIN family_individual as fi ON fi.family_id=f.account_info_id ' +
      'INNER JOIN individual_account as i ON i.account_info_id=fi.individual_id ' +
      'INNER JOIN account_information as ai ON ai.account_id=i.account_info_id ' +
      'INNER JOIN account_information as af ON af.account_id=f.account_info_id ' +
      'WHERE f.account_info_id=?';
    try {
      const [rows] = await connection.query(sql, family_account_id);
      return rows as RowDataPacket[];
    } catch (err) {
      console.log(err);
      throw err;
    }
  }


  /*
  answer example
  [
  
      
    {
        "family_account_id": 1,
        "context": "acto pashido",
        "first_name": "hadar",
        "last_name": "liber",
        "address_id": 1,
        "individual_account_id": 1,
        "email": "hh@gmail.com",
        "person_id": 12113414,
        "individual_currency": "uk",
        "individual_balance": 6700,
        "individual_status": "active",
        "individual_agent_id": 1,
         "family_currency": "uk",
        "family_balance": 6700,
        "family_status": "active",
        "family_agent_id": 1,
        
    }
]*/

  async getFamilyByIdDB(family_id: number): Promise<RowDataPacket[]> {
    const sql =
      'SELECT * FROM account_information as ai' +
      'INNER JOIN family_account as fa ON fa.account_info_id=ai.account_id ' +
      'WHERE ai.account_id=?';

    try {
      const [rows] = await connection.query(sql, family_id);
      return rows as RowDataPacket[];
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async removeIndividualsFromFamilyDB(
    family_account_id: number,
    individuals_account_id: number[],
  ): Promise<RowDataPacket[]> {
    const primary_ids: number[] = (
      (await this.getPrimaryIdsToDeleteIndividualDB(
        family_account_id,
        individuals_account_id,
      )) as any
    )[0];
    const sql =
      'DELETE FROM family_individual ' +
      `WHERE family_individual_id IN (${'?' + ',?'.repeat(individuals_account_id.length - 1)})`;
    try {
      await connection.query(sql, primary_ids);
      const rows = await this.getFamilyWithOwnersByIdDB(family_account_id);
      return rows;
    } catch (err) {
      console.log(err);
      throw err;
    }
  }


  async getPrimaryIdsToDeleteIndividualDB(
    family_account_id: number,
    individuals_account_id: number[],
  ): Promise<RowDataPacket[]> {
    const sql =
      'SELECT family_individual_id FROM family_individual ' +
      `WHERE family_id=? AND individual_id IN (${
        '?' + ',?'.repeat(individuals_account_id.length - 1)
      })`;
    try {
      const [rows] = await connection.query(sql, [family_account_id, ...individuals_account_id]);
      return rows as RowDataPacket[];
    } catch (err) {
      console.log(err);
      throw err;
    }
  }
}

export default new FamilyRepository();
