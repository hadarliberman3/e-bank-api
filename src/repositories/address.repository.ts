import { RowDataPacket, ResultSetHeader } from 'mysql2';
import { connection } from '../db/sql.connection.js';
import { IAddressDB } from '../models/account.db.model.js';

class AddressRepository {
  async createAdressDB(address: IAddressDB): Promise<RowDataPacket[]> {
    const sql = 'INSERT INTO address SET ?';
    try {
      const rows = await connection.query(sql, address);
      const inserted_address_id: number = Number((rows[0] as ResultSetHeader).insertId);
      return await this.getAddressesByIdDB([inserted_address_id]);
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async getAddressesByIdDB(addresses_id: number[]): Promise<RowDataPacket[]> {
    const sql = `SELECT * FROM address WHERE address_id IN (${
      '?' + ',?'.repeat(addresses_id.length - 1)
    })`;
    try {
      console.log(sql);
      const [rows] = await connection.query(sql, addresses_id);
      console.log(rows);
      return rows as RowDataPacket[];
    } catch (err) {
      console.log(err);
      throw err;
    }
  }
}
export default new AddressRepository();
