import { RowDataPacket } from 'mysql2';
import { connection } from '../db/sql.connection.js';
import { IIndividualAccountDB } from '../models/account.db.model.js';

class IndividualRepository {
  async createIndividualDB(individual: IIndividualAccountDB): Promise<RowDataPacket[]> {
    const sql = 'INSERT INTO individual_account SET ?';

    try {
      await connection.query(sql, individual);
      return await this.getIndividualsByIdDB([individual.account_info_id as number]);
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async getIndividualsByIdDB(individuals_id: number[]): Promise<RowDataPacket[]> {
    console.log(individuals_id);
    const sql =
      `SELECT * FROM account_information as ai ` +
      'INNER JOIN individual_account as i ON i.account_info_id=ai.account_id ' +
      'INNER JOIN address as ad ON ad.address_id=i.address_id ' +
      `WHERE ai.account_id IN (${'?' + ',?'.repeat(individuals_id.length - 1)})`;

    try {
      const [rows] = await connection.query(sql, individuals_id);
      console.log(rows);
      return rows as RowDataPacket[];
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async getAllIndividualDevDB(): Promise<RowDataPacket[]> {
    const sql =
      'SELECT * FROM account_infromation as ai ' +
      'INNER JOIN individual_account as i ON i.account_info_id=ai.account_id ' +
      'INNER JOIN address as ad ON ad.address_id=i.address_id ';

    try {
      const [rows] = await connection.query(sql);
      console.log(rows);
      return rows as RowDataPacket[];
    } catch (err) {
      console.log(err);
      throw err;
    }
  }
}
export default new IndividualRepository();
