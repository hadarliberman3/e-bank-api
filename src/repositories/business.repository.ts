import { RowDataPacket } from 'mysql2';
import { connection } from '../db/sql.connection.js';
import { IBusinessAccountDB } from '../models/account.db.model.js';

class BusinessRepository {
  async createBusinessDB(business: IBusinessAccountDB): Promise<RowDataPacket[]> {
    const sql = 'INSERT INTO business_account SET ?';
    try {
      await connection.query(sql, business);
      // console.log(rows);
      // const inserted_business_id: number = Number((rows[0] as ResultSetHeader).insertId);
      return await this.getBusinessesByIdDB([business.account_info_id as number]);
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async getBusinessesByIdDB(businesses_id: number[]): Promise<RowDataPacket[]> {
    const sql =
      'SELECT * FROM account_information as a ' +
      'INNER JOIN business_account as b ON b.account_info_id=a.account_id ' +
      'INNER JOIN address as ad ON ad.address_id=b.address_id ' +
      `WHERE a.account_id IN (${'?' + ',?'.repeat(businesses_id.length - 1)})`;
    try {
      const [rows] = await connection.query(sql, businesses_id);
      return rows as RowDataPacket[];
    } catch (err) {
      console.log(err);
      throw err;
    }
  }
}
export default new BusinessRepository();
