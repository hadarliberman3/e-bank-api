import { RowDataPacket, ResultSetHeader } from 'mysql2';
import { connection } from '../db/sql.connection.js';
import { IAccountDB } from '../models/account.db.model.js';

class AccountRepository {
  async createAccountDB(account: IAccountDB): Promise<RowDataPacket[]> {
    const sql = 'INSERT INTO account_information SET ?';
    try {
      const rows = await connection.query(sql, account);
      const inserted_account_id: number = Number((rows[0] as ResultSetHeader).insertId);
      return await this.getAccountsByIdDB([inserted_account_id]);
    } catch (err) {
      console.log(err);
      throw err;
    }
  }
  async getAccountsByIdDB(accounts_id: number[]): Promise<RowDataPacket[]> {
    const sql = `SELECT * FROM account_information WHERE account_id IN (${
      '?' + ',?'.repeat(accounts_id.length - 1)
    })`;
    try {
      const [rows] = await connection.query(sql, accounts_id);
      return rows as RowDataPacket[];
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  //return array of ids and the first element is tha status
  async updateStatusesAccountsByIdDB(accounts_id: any[], status: string): Promise<RowDataPacket[]> {
    try {
      const sql = `UPDATE account_information SET status=? WHERE account_id IN (${
        '?' + ',?'.repeat(accounts_id.length - 1)
      })`;
      await connection.query(sql, [status, ...accounts_id]);
      accounts_id.push(status);
      return accounts_id as RowDataPacket[];
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async UpdateAccountAmountByIdDB(account_id: number, balance: number): Promise<RowDataPacket[]> {
    const sql = 'UPDATE account_information SET ? WHERE account_id=?';
    try {
      await connection.query(sql, [{ balance }, account_id]);
      const rows = await this.getAccountsByIdDB([account_id]);
      return rows;
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async Transaction(
    src_id: number,
    dest_id: number,
    src_balance: number,
    dest_balance: number,
  ): Promise<RowDataPacket[]> {
    const sql =
      'UPDATE account_information SET balance = ' +
      'CASE WHEN account_id = ? THEN ? WHEN account_id = ? THEN ? ' +
      'END'+
      'WHERE account_id = ? OR account_id = ?';
    try {
      // await connection.beginTransaction();
      await connection.query(sql, [src_id, src_balance, dest_id, dest_balance, src_id, dest_id]);
      // await connection.commit();
      const rows = await this.getAccountsByIdDB([src_id, dest_id]);
      return rows;
    } catch (err) {
      // await connection.rollback();
      console.log(err);
      throw err;
    }
  }
}
export default new AccountRepository();
