import { HttpException } from './http.exception.js';

export class TransactionException extends HttpException {
  constructor(public status: number, public message: string) {
    super(400, 'Transaction failed');
  }
}
