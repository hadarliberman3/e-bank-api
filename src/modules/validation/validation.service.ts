import { IErrorItem, IRule, IValidationItem, IValidationFunctions, IGeneralObject } from './validation.model.js';


export class InputValidationService {
  private static valdiation_functions: IValidationFunctions = {
    isExist: (input: any, val: boolean): boolean => !!input === val,
    isPositive: (input: any, val: boolean): boolean => Number(input) >= 0 === val,
    isNumber: (input: any, val: boolean): boolean => !isNaN(Number(input)) === val,
    greaterThan: (input: number, val: number): boolean => Number(input) >= val,
    numOfdigits: (input: number, val: number): boolean =>
      input !== undefined && !isNaN(Number(input)) && String(input).length === val,
    isEmptyArray: (input: any, val: boolean): boolean =>
      Array.isArray(input) && (input.length === 0) === val,
    isIn: (input: any, val: any[]): boolean => val.includes(input),
    isNumbersArray: (input: any, val: boolean): boolean =>
      Array.isArray(input) && input.every((ele: any) => !isNaN(Number(ele))) === val,
  };

  public static configureValidator(extended_functions: IValidationFunctions): void {
    InputValidationService.valdiation_functions = {
      ...InputValidationService.valdiation_functions,
      ...extended_functions
    };
  }

  private static getErrorList = (item: IValidationItem, input: any): IErrorItem[] => {
    const errorItems: IErrorItem[] = [];
    item.rules.forEach((rule: IRule) => {
      const operationType = Object.keys(rule)[0];
      const operationLimit = rule[operationType];
      const operatorFunc = InputValidationService.valdiation_functions[operationType];
      const passes = operatorFunc(input, operationLimit);
      if (passes) return true;
      else
        errorItems.push({
          content: rule.validation_message,
          info: 'Validation Error',
        });
    });
    return errorItems;
  };

  public static getValidationResult = (validation_items: IValidationItem[], validation_object: IGeneralObject): IErrorItem[] =>
    validation_items.reduce((acc: IErrorItem[], item: IValidationItem): IErrorItem[] => {
      const field: any = validation_object[item.key];
      const errItems = InputValidationService.getErrorList(item, field);
      const inputPassesValidation = field && item.rules && errItems.length < 1;
      return !inputPassesValidation ? [...acc, ...errItems] : acc;
    }, []);
}




export default new InputValidationService();
