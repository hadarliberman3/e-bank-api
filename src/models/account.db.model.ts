export interface IAccountDB {
  account_id?: number | null;
  currency: string;
  balance: number;
  status: string;
  type: string;
  agent_id: number;
}

export interface IIndividualAccountDB {
  individual_id?: number;
  person_id: number;
  first_name: string;
  last_name: string;
  address_id: number;
  email: string;
  account_info_id?: number;
}

export interface IBusinessAccountDB {
  business_id?: number;
  company_id: number;
  company_name: string;
  context: string;
  address_id: number;
  account_info_id?: number;
}

export interface IFamilyAccountDB {
  family_id?: number;
  context: string;
  account_info_id?: number;
}

export interface IAddressDB {
  address_id?: number;
  country_name: string;
  country_code: string;
  postal_code: number;
  city: string;
  region: string;
  street_name: string;
  street_number: number;
}
