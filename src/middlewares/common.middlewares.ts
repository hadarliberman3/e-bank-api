import fs from 'fs';
import { RequestHandler } from 'express';

export const httpLogger = (path: string): RequestHandler => {
  const httpFile = fs.createWriteStream(path, { flags: 'a' });
  return (req, res, next): void => {
    httpFile.write(`${path} [${Date.now()}] ${req.method} ${req.path} \n`);
    next();
  };
};

export default function (func: RequestHandler): RequestHandler {
  return async function (req, res, next) {
    try {
      await func(req, res, next);
    } catch (err) {
      console.log('caught error------------------------------------------------');
      next(err);
    }
  };
}
