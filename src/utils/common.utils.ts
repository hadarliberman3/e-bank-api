export const curry = (f: Function): Function => {
  // curry(f) does the currying transform
  return (a: any): Function => {
    return (b: any): Function => {
      return f(a, b) as Function;
    };
  };
};
