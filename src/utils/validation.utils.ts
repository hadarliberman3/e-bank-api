export const EBANK_VALIDATION_FUNCTIONS = {
  isNumberBiTuples: (input: any, val: boolean): boolean =>
    (Array.isArray(input) &&
      input.reduce((acc: boolean, tuple) => {
        return (
          acc &&
          Array.isArray(tuple) &&
          tuple.length === 2 &&
          !isNaN(Number(tuple[0])) &&
          !isNaN(Number(tuple[1]))
        );
      }, true)) === val,
  biTuplesSumGreaterThan: (input: any, val: number): boolean =>
    Array.isArray(input) &&
    input.reduce((acc: boolean, tuple) => {
      return (
        acc &&
        Array.isArray(tuple) &&
        tuple.length === 2 &&
        !isNaN(Number(tuple[0])) &&
        !isNaN(Number(tuple[1]))
      );
    }, true) &&
    input.reduce((acc: number, tuple: [number, number]) => acc + Number(tuple[1]), 0) > val,
  biTuplesAmountPositive: (input: any, val: boolean): boolean =>
    (Array.isArray(input) &&
      input.reduce((acc: boolean, tuple) => {
        return (
          acc &&
          Array.isArray(tuple) &&
          tuple.length === 2 &&
          !isNaN(Number(tuple[0])) &&
          !isNaN(Number(tuple[1])) &&
          Number(tuple[1]) >= 0
        );
      }, true)) === val,
};
