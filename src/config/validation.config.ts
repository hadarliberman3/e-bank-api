import { IGeneralObject } from '../modules/validation/validation.model.js';

export const validation_config_object: IGeneralObject = {
  general: {
    active_deactive_account: [
      {
        key: 'status',
        rules: [
          {
            isExist: true,
            validation_message: 'Status is mandatory',
          },
          {
            isIn: ['active', 'deactivate'],
            validation_message: 'Status must be a either active or deactive',
          },
        ],
      },
      {
        key: 'account_ids',
        rules: [
          {
            isExist: true,
            validation_message: 'Account ids is mandatory',
          },
          {
            isEmptyArray: false,
            validation_message: 'Account ids must be a non-empty array',
          },
          {
            isNumbersArray: true,
            validation_message: 'Ids must be numbers',
          },
        ],
      },
    ],
    transfer: [
      {
        key: 'amount',
        rules: [
          {
            isExist: true,
            validation_message: 'Amount is mandatory',
          },
          {
            isNumber: true,
            validation_message: 'Amount should be a number',
          },
          {
            isPositive: true,
            validation_message: 'Amount should be a positive number',
          },
        ],
      },
      {
        key: 'src',
        rules: [
          {
            isExist: true,
            validation_message: 'src is mandatory',
          },
        ],
      },
      {
        key: 'dest',
        rules: [
          {
            isExist: true,
            validation_message: 'dest is mandatory',
          },
        ],
      },
    ],
  },
  individual: {
    create_account: [
      {
        key: 'individual_id',
        rules: [
          {
            isExist: true,
            validation_message: 'Individual ID is mandatory',
          },
          {
            isNumber: true,
            validation_message: 'Individual ID should be a number',
          },
          {
            greaterThan: 1000000,
            validation_message: 'Individual ID should be greater than 1000000',
          },
          {
            numOfDigits: 7,
            validation_message: 'Individual ID should have 7 digits',
          },
        ],
      },
      {
        key: 'first_name',
        rules: [
          {
            isExist: true,
            validation_message: 'First name is mandatory',
          },
        ],
      },
      {
        key: 'last_name',
        rules: [
          {
            isExist: true,
            validation_message: 'Last name is mandatory',
          },
        ],
      },
      {
        key: 'currency',
        rules: [
          {
            isExist: true,
            validation_message: 'Last name is mandatory',
          },
        ],
      },
      {
        key: 'account_id',
        rules: [
          {
            isExist: false,
            validation_message: 'account_id should not be provided',
          },
        ],
      },
    ],
    get_account: [
      {
        key: 'account_id',
        rules: [
          {
            isExist: true,
            validation_message: 'Account ID is mandatory',
          },
          {
            isNumber: true,
            validation_message: 'Account ID must be a number',
          },
        ],
      },
    ],
  },
  business: {
    create_account: [
      {
        key: 'company_id',
        rules: [
          {
            isExist: true,
            validation_message: 'Company ID is mandatory',
          },
          {
            isNumber: true,
            validation_message: 'Company ID should be a number',
          },
          {
            greaterThan: 10000000,
            validation_message: 'Company ID should be greater than 10,000000',
          },
          {
            numOfDigits: 8,
            validation_message: 'Company ID should have 7 digits',
          },
        ],
      },
      {
        key: 'company_name',
        rules: [
          {
            isExist: true,
            validation_message: 'Company name is mandatory',
          },
        ],
      },
      {
        key: 'currency',
        rules: [
          {
            isExist: true,
            validation_message: 'Currency is mandatory',
          },
        ],
      },
    ],
    get_account: [
      {
        key: 'account_id',
        rules: [
          {
            isExist: true,
            validation_message: 'Account ID is mandatory',
          },
          {
            isNumber: true,
            validation_message: 'Account ID must be a number',
          },
        ],
      },
    ],
  },
  family: {
    create_account: [
      {
        key: 'currency',
        rules: [
          {
            isExist: true,
            validation_message: 'Currency is mandatory',
          },
        ],
      },
      {
        key: 'owner_tuples',
        rules: [
          {
            isExist: true,
            validation_message: 'Owners accounts tuples is mandatory',
          },
          {
            isNumberBiTuples: true,
            validation_message: 'Owner tuples must be an array of tuples  [account_id, amount ]',
          },
          {
            biTuplesAmountPositive: true,
            validation_message: 'Owner amounts should be positive',
          },
          {
            biTuplesSumGreaterThan: 5000,
            validation_message: 'Owner tuples amount sum should be greater than 5000',
          },
        ],
      },
    ],
    get_account: [
      {
        key: 'account_id',
        rules: [
          {
            isExist: true,
            validation_message: 'Account ID is mandatory',
          },
          {
            isNumber: true,
            validation_message: 'Account ID must be a number',
          },
        ],
      },
    ],
    add_members: [
      {
        key: 'family_account_id',
        rules: [
          {
            isExist: true,
            validation_message: 'Account ID is mandatory',
          },
          {
            isNumber: true,
            validation_message: 'Account ID must be a number',
          },
        ],
      },
      {
        key: 'owner_tuples',
        rules: [
          {
            isExist: true,
            validation_message: 'Owner tuples is mandatory',
          },
          {
            isNumberBiTuples: true,
            validation_message: 'Owners tuples should be an array of tuples',
          },
          {
            isEmptyArray: false,
            validation_message: 'Owners array should not be empty',
          },
          {
            biTuplesAmountPositive: true,
            validation_message: 'Owners tuple amunts should be positive',
          },
        ],
      },
    ],
    remove_members: [
      {
        key: 'family_account_id',
        rules: [
          {
            isExist: true,
            validation_message: 'Account ID is mandatory',
          },
          {
            isNumber: true,
            validation_message: 'Account ID must be a number',
          },
        ],
      },
      {
        key: 'owner_tuples',
        rules: [
          {
            isExist: true,
            validation_message: 'Owner tuples is mandatory',
          },
          {
            isNumberBiTuples: true,
            validation_message: 'Owners tuples should be an array of tuples',
          },
          {
            isEmptyArray: false,
            validation_message: 'Owners array should not be empty',
          },
          {
            biTuplesAmountPositive: true,
            validation_message: 'Owners tuple amunts should be positive',
          },
        ],
      },
    ],
    close_family: [
      {
        key: 'account_id',
        rules: [
          {
            isExist: true,
            validation_message: 'Account ID is mandatory',
          },
          {
            isNumber: true,
            validation_message: 'Account ID must be a number',
          },
        ],
      },
    ],
  },
};
