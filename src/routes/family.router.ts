import express from 'express';
import family_controller from '../controllers/family.controller.js';
import raw from '../middlewares/common.middlewares.js';

const family_router = express.Router();

family_router.post('/', raw(family_controller.createFamily));

family_router.get('/:id', raw(family_controller.getFamily));

family_router.get('/', raw(family_controller.getAllFamillies));

export default family_router;
