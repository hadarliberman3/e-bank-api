import express from 'express';
import business_controller from '../controllers/business.controller.js';
import raw from '../middlewares/common.middlewares.js';

const business_router = express.Router();

business_router.post('/', raw(business_controller.createBusiness));

business_router.get('/:id', raw(business_controller.getBusiness));

business_router.get('/', raw(business_controller.getAllBusinesses));

export default business_router;
