import express from 'express';
import accountController from '../controllers/account.controller.js';

import raw from '../middlewares/common.middlewares.js';

const account_router = express.Router();

// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
account_router.put('/status', raw(accountController.setStatus));

export default account_router;
