import { IAccount, IAddress, IIndividualAccount } from '../models/account.model.js';
import account_repositpry from '../repositories/account.repository.js';
import individual_repository from '../repositories/individual.repository.js';
import address_repository from '../repositories/address.repository.js';
import individual_converter from '../converters/individual.converter.js';
import account_converter from '../converters/account.converter.js';
import address_converter from '../converters/address.converter.js';
import { RowDataPacket } from 'mysql2';
class IndividualService {
  async create(individual: IIndividualAccount): Promise<IIndividualAccount | null> {
    //*********************** */
    //TO DO: validation here
    //*********************** */

    const db_models = individual_converter.individualModelToDb(individual);
    const [account_db_model, address_db_model, individual_db_model] = db_models;

    //create account in db
    const account_db_result: RowDataPacket[] = await account_repositpry.createAccountDB(
      account_db_model,
    );

    console.log('before create address');
    //create address in db
    const address_db_result = await address_repository.createAdressDB(address_db_model);
    console.log('after create address');

    //convert db results
    const account_info: IAccount = account_converter.rowDataPacketToModel(account_db_result);
    console.log('converting');
    const address_info: IAddress = address_converter.rowDataPacketToModel(address_db_result);
    console.log('after converting address db');

    console.log('address info');
    console.log(address_info);

    //prepare individual db model
    individual_db_model.account_info_id = account_info.account_id;
    individual_db_model.address_id = address_info.address_id as number;

    //create individual in db
    const individual_db_result = await individual_repository.createIndividualDB(
      individual_db_model,
    );
    console.log('before converting individual db ');
    //convert db result to individual model
    const saved_individual = individual_converter.rowDataPacketToModel(individual_db_result);

    return saved_individual;
  }

  async findById(individual_id: number): Promise<IIndividualAccount | null> {
    console.log('hello from service');
    console.log(individual_id);
    const individual_db_result = await individual_repository.getIndividualsByIdDB([individual_id]);
    console.log(individual_db_result);
    individual_db_result.forEach(console.log);
    //*********************** */
    //TO DO: validation here
    //*********************** */

    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    let individual = individual_converter.rowDataPacketToModel(individual_db_result);
    console.log(individual);
    return individual;
  }

  async isIndividual(individual_id: number): Promise<Boolean> {
    const found_individual = await this.findById(individual_id);
    if (found_individual === null) {
      return false;
    } else {
      return true;
    }
  }
}
const account_service = new IndividualService();
export default account_service;
