/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */

import account_repository from '../repositories/account.repository.js';
import individual_service from '../services/individual.service.js';
import business_service from '../services/business.service.js';
import { IAccount } from '../models/account.model.js';
class AccountService {
  async setStatus(primary_ids: number[], status: string) {
    //get for each account the account info id
    const pending_account_ids = primary_ids.map(async (primaryId): Promise<number> => {
      let not_found = -1;
      let foundAccount: IAccount | null = null;
      const isIndividual = await individual_service.isIndividual(primaryId);
      const isBusiness = await business_service.isBusiness(primaryId);

      if (isIndividual) {
        foundAccount = await individual_service.findById(primaryId);
      } else if (isBusiness) {
        foundAccount = await business_service.findById(primaryId);
      }

      return foundAccount?.account_id || not_found;
    });

    const account_ids = await Promise.all(pending_account_ids);
    await account_repository.updateStatusesAccountsByIdDB(account_ids, status);

    return;
  }
}

const account_service = new AccountService();
export default account_service;
