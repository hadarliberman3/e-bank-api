import { IAccount, IAddress, IBusinessAccount } from '../models/account.model.js';
import account_repositpry from '../repositories/account.repository.js';
import business_repository from '../repositories/business.repository.js';
import address_repository from '../repositories/address.repository.js';
import business_converter from '../converters/business.converter.js';
import account_converter from '../converters/account.converter.js';
import address_converter from '../converters/address.converter.js';
import { RowDataPacket } from 'mysql2';
class BusinessService {
  async create(business: IBusinessAccount): Promise<IBusinessAccount | null> {
    //*********************** */
    //TO DO: validation here
    //*********************** */

    const db_models = business_converter.businessModelToDb(business);
    const [account_db_model, address_db_model, business_db_model] = db_models;

    //create account in db
    const account_db_result: RowDataPacket[] = await account_repositpry.createAccountDB(
      account_db_model,
    );

    console.log('before create address');
    //create address in db
    const address_db_result = await address_repository.createAdressDB(address_db_model);
    console.log('after create address');

    //convert db results
    const account_info: IAccount = account_converter.rowDataPacketToModel(account_db_result);

    console.log('ACCOUNT info');
    console.log(account_info);

    console.log('converting');
    const address_info: IAddress = address_converter.rowDataPacketToModel(address_db_result);
    console.log('after converting address db');

    console.log('address info');
    console.log(address_info);

    //prepare business db model
    business_db_model.account_info_id = account_info.account_id;
    business_db_model.address_id = address_info.address_id as number;

    //create business in db
    const business_db_result = await business_repository.createBusinessDB(business_db_model);
    console.log('before converting business db ');
    //convert db result to business model
    const saved_business = business_converter.rowDataPacketToModel(business_db_result);

    return saved_business;
  }

  async findById(business_id: number): Promise<IBusinessAccount | null> {
    console.log('hello from service');
    console.log(business_id);
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    const business_db_result = await business_repository.getBusinessesByIdDB([business_id]);
    console.log(business_db_result);
    //business_db_result.forEach(console.log);
    //*********************** */
    //TO DO: validation here
    //*********************** */

    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    const business = business_converter.rowDataPacketToModel(business_db_result);
    console.log('business is');
    console.log(business);

    return business;
  }

  async isBusiness(business_id: number): Promise<Boolean> {
    const found_business = await this.findById(business_id);
    if (found_business === null) {
      return false;
    } else {
      return true;
    }
  }
}
const business_service = new BusinessService();
export default business_service;
